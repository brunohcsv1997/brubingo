CREATE DATABASE brubingo;

USE brubingo;

CREATE TABLE users (
	`Id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    `NickName` VARCHAR(500) NOT NULL,
    `Password` VARCHAR(500) NOT NULL,
    `Active` BIT(1) DEFAULT 1 NOT NULL,
    `CreatedAt` DATETIME DEFAULT NOW()
);	
ALTER TABLE users CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;

CREATE TABLE games (
	`Id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    `Name` VARCHAR(500) NOT NULL,
    `Code` VARCHAR(300) NOT NULL,
    `Owner_Id` INT UNSIGNED NOT NULL,
    `AutoCall` BIT(1) DEFAULT 0 NOT NULL,
    `RefreshTime` TINYINT UNSIGNED NOT NULL,
    `CreatedAt` DATETIME DEFAULT NOW() NOT NULL,
    `FinishedAt` DATETIME DEFAULT NULL
);

ALTER TABLE games CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;

CREATE TABLE balls (
	`Id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    `Number` SMALLINT UNSIGNED NOT NULL,
    `Game_Id` INT UNSIGNED NOT NULL,
    `CreatedAt` DATETIME DEFAULT NOW() NOT NULL,
    `CalledAt` DATETIME DEFAULT NULL
);

CREATE TABLE games_guests (
	`Id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	`User_Id` INT UNSIGNED NOT NULL,
    `Game_Id` INT UNSIGNED NOT NULL,
    `CreatedAt` DATETIME DEFAULT NOW() NOT NULL
);

CREATE INDEX IDX_users_NickName ON users(`NickName`);
CREATE INDEX IDX_users_Password ON users(`Password`);
CREATE INDEX IDX_users_CreatedAt ON users(`CreatedAt`);
CREATE INDEX IDX_users_Active ON users(`Active`);
CREATE INDEX IDX_games_Name ON games(`Name`);
CREATE INDEX IDX_games_Code ON games(`Code`);
CREATE INDEX IDX_games_Owner_Id ON games(`Owner_Id`);
CREATE INDEX IDX_games_CreatedAt ON games(`CreatedAt`);
CREATE INDEX IDX_games_FinishedAt ON games(`FinishedAt`);
CREATE INDEX IDX_games_RefreshTime ON games(`RefreshTime`);
CREATE INDEX IDX_games_AutoCall ON games(`AutoCall`);
CREATE UNIQUE INDEX UK_games_Code ON games(`Code`);
CREATE INDEX IDX_balls_Number ON balls(`Number`);
CREATE INDEX IDX_balls_Game_Id ON balls(`Game_Id`);
CREATE INDEX IDX_balls_CreatedAt ON balls(`CreatedAt`);
CREATE INDEX IDX_balls_CalledAt ON balls(`CalledAt`);
CREATE INDEX IDX_games_guests_User_Id ON games_guests(`User_Id`);
CREATE INDEX IDX_games_guests_Game_Id ON games_guests(`Game_Id`);
CREATE INDEX IDX_games_guests_CreatedAt ON games_guests(`CreatedAt`);
CREATE UNIQUE INDEX IDX_games_guests_User_Game ON games_guests(`User_Id`, `Game_Id`);

ALTER TABLE games
ADD CONSTRAINT FK_games_Owner_Id
FOREIGN KEY (Owner_Id) REFERENCES users(Id);

ALTER TABLE balls
ADD CONSTRAINT FK_balls_Game_Id
FOREIGN KEY (Game_Id) REFERENCES games(Id);

ALTER TABLE games_guests
ADD CONSTRAINT FK_games_guests_Game_Id
FOREIGN KEY (Game_Id) REFERENCES games(Id);

ALTER TABLE games_guests
ADD CONSTRAINT FK_games_guests_User_Id
FOREIGN KEY (User_Id) REFERENCES users(Id);