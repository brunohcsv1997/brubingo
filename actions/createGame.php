<?php 
    include_once "../configs/dbConfig.php";
    @session_start();
    if (!$_SESSION['User_Id']) {
        header('Location: ../pages/login.php');
    }

    $Name = str_replace('"', '/"', str_replace("'", "/'", $_POST['name']));
    $QtyBalls = str_replace('"', '/"', str_replace("'", "/'", $_POST['qtyBalls']));
    $AutoCall = isset($_POST['autoCall']) ? 1 : 0;

    if (strlen($Name) == 0 || strlen($QtyBalls) == 0) {
        header("Location: ../pages/menu.php?erro=1");
    } else {

        $createGame = "INSERT INTO games (`Name`, `AutoCall`, `Owner_Id`, `Code`, `RefreshTime`) VALUES ('$Name', $AutoCall , $_SESSION[User_Id], 
            CONCAT('$_SESSION[NickName]_',LEFT(SHA1(NOW()),8)), $_POST[refreshTime])";
        $conn->query($createGame);

        $getCreatedGame = "SELECT `Code`, `Id` FROM games WHERE Owner_Id = $_SESSION[User_Id] ORDER BY `Id` DESC LIMIT 1";

        $result = $conn->query($getCreatedGame);

        $conn->query("start transaction;");
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {

                $insertBalls = "INSERT INTO balls (`Number`, `Game_Id`) VALUES ";

                    for($i = 1; $i <= $QtyBalls; ++$i) {
                        $insertBalls .= ", ($i, $row[Id])";
                    }

                $insertBalls = str_replace("VALUES ,", "VALUES ", $insertBalls);

                $conn->query($insertBalls);
                $conn->query("commit");
                header("Location: ../pages/game.php?Code=". $row['Code']);

            }
        }
    }