<?php 
    include_once "../configs/dbConfig.php";

    @session_start();
    if (!$_SESSION['User_Id']) {
        header('Location: ../pages/login.php');
    }

    $updateAutoCall = "UPDATE games 
        SET AutoCall = 1
        WHERE Id = $_GET[GameId]
        AND Owner_Id = $_SESSION[User_Id]
    ";

    $getCode = "SELECT Code FROM games WHERE Id = $_GET[GameId]";

    $conn->query($updateAutoCall);
    
    $resultGetCode = $conn->query($getCode);

    if ($resultGetCode->num_rows > 0) {
        while($row = $resultGetCode->fetch_assoc()) {
            header("Location: ../pages/game.php?Code=$row[Code]");
        }
    }