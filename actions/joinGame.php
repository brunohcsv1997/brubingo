<?php 
    include_once "../configs/dbConfig.php";

    @session_start();
    if (!$_SESSION['User_Id']) {
        header('Location: ../pages/login.php');
    }
    
    $getGame = "SELECT `Id`, `Code`, `Owner_Id` FROM games WHERE Code LIKE '$_POST[Code]' ORDER BY 1 DESC LIMIT 1";

    $resultGetGame = $conn->query($getGame);

    if ($resultGetGame->num_rows > 0) {
        while($row = $resultGetGame->fetch_assoc()) {
            if ($_SESSION['User_Id'] != $row['Owner_Id']){
                $conn->query("INSERT IGNORE INTO games_guests (User_Id, Game_Id) VALUES ($_SESSION[User_Id], $row[Id]);");
            }
            header("Location: ../pages/game.php?Code=$row[Code]");
        }
    }
