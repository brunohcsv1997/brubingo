<?php 
    include_once "../configs/dbConfig.php";

    @session_start();
    if (!$_SESSION['User_Id']) {
        header('Location: ../pages/login.php');
    }

    $countBallsToFinish = "SELECT b.Number FROM balls b 
        JOIN games g ON g.Id = b.Game_Id AND g.Id = $_GET[GameId] AND b.CalledAt IS NULL";

    $resultBallsToFinish = $conn->query($countBallsToFinish);

    if ($resultGetCode->num_rows > 0){
        $callBall = "UPDATE balls 
            SET CalledAt = NOW()
            WHERE Game_Id = $_GET[GameId]
            AND Game_Id IN (SELECT Id FROM games WHERE FinishedAt IS NULL AND Owner_Id = $_SESSION[User_Id])
            AND CalledAt IS NULL
            ORDER BY RAND() LIMIT 1
        ";

        $conn->query($callBall);
    } else {
        $finishGame = "UPDATE games SET FinishedAt = NOW() WHERE Id = $_GET[GameId] AND Owner_Id = $_SESSION[User_Id]";
        $conn->query($finishGame);
    }

    $getCode = "SELECT Code FROM games WHERE Id = $_GET[GameId]";

    $resultGetCode = $conn->query($getCode);

    if ($resultGetCode->num_rows > 0) {
        while($row = $resultGetCode->fetch_assoc()) {
            header("Location: ../pages/game.php?Code=$row[Code]");
        }
    }