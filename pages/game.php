<?php 
    include_once "../configs/dbConfig.php";
    @session_start();

    if (!$_SESSION['User_Id']) {
        header('Location: pages/login.php');
    }

    $queryGame = "SELECT `Id`, 
        `Name`, 
        `Code`, 
        DATE_FORMAT(`CreatedAt`, '%d/%m/%Y %H:%i:%s') 'CreatedAt', 
        DATE_FORMAT(`FinishedAt`, '%d/%m/%Y %H:%i:%s') 'FinishedAt',
        CASE WHEN `FinishedAt` IS NULL THEN 1 ELSE 0 END 'Active',
        `AutoCall`,
        `Owner_Id`,
        `RefreshTime`
        FROM games WHERE `Code` = '$_GET[Code]' ORDER BY 1 DESC LIMIT 1";

    $queryBalls = "SELECT b.`Id`, 
        b.`Number`, 
        CASE WHEN b.`CalledAt` IS NULL THEN 0 ELSE 1 END 'Called'
        FROM balls b
        JOIN games g ON g.Id = b.Game_Id 
        WHERE g.Code = '$_GET[Code]' ORDER BY b.`Number` ASC";

    
    $queryLastBall = "SELECT b.`Id`, 
        b.`Number`, 
        CASE WHEN b.`CalledAt` IS NULL THEN 0 ELSE 1 END 'Called'
        FROM balls b
        JOIN games g ON g.Id = b.Game_Id AND b.`CalledAt` IS NOT NULL
        WHERE g.Code = '$_GET[Code]' ORDER BY b.`CalledAt` DESC LIMIT 1";

    $resultGame = $conn->query($queryGame);

    if ($resultGame->num_rows > 0) {
        while($row = $resultGame->fetch_assoc()) {
            $Id = $row['Id'];
            $Name = $row['Name'];
            $Code = $row['Code'];
            $Active = $row['Active'];
            $AutoCall = $row['AutoCall'];
            $Owner_Id = $row['Owner_Id'];
            $RefreshTime = $row['RefreshTime'];
        }
    }

    if ($AutoCall == 1 && $_SESSION['User_Id'] == $Owner_Id){
        $callBall = "UPDATE balls 
            SET CalledAt = NOW()
            WHERE Game_Id = $Id AND CalledAt IS NULL 
            AND Game_Id IN (SELECT Id FROM games WHERE FinishedAt IS NULL)
            ORDER BY RAND() LIMIT 1";

        $countBallsToFinish = "SELECT b.Number FROM balls b 
            JOIN games g ON g.Id = b.Game_Id AND g.Id = $Id AND b.CalledAt IS NULL";
        $conn->query($callBall);

        $resultBallsToFinish = $conn->query($countBallsToFinish);

        if ($resultBallsToFinish->num_rows == 0){
            $finishGame = "UPDATE games SET FinishedAt = NOW() WHERE Id = $Id AND Owner_Id = $_SESSION[User_Id]";
            $conn->query($finishGame);
        }
    }

    $resultBalls = $conn->query($queryBalls);
    $resultLastBall = $conn->query($queryLastBall);

    if ($resultLastBall->num_rows > 0) {
        while($row = $resultLastBall->fetch_assoc()) {
            $LastBall = $row['Number'];
        } 
    } else {
        $LastBall = " ";
    }

?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>

    .login-page {
    width: 20%;
    padding: 8% 0 0;
    margin: auto;
    }
    .form {
    position: relative;
    z-index: 1;
    background: #FFFFFF;
    max-width: 100%;
    margin: 0 auto 100px;
    padding: 45px;
    text-align: center;
    box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
    }
    .form input {
    font-family: "Roboto", sans-serif;
    outline: 0;
    background: #f2f2f2;
    width: 100%;
    border: 0;
    margin: 0 0 15px;
    padding: 15px;
    box-sizing: border-box;
    font-size: 14px;
    }
    .form button {
    font-family: "Roboto", sans-serif;
    text-transform: uppercase;
    outline: 0;
    width: 100%;
    border: 0;
    padding: 15px;
    font-size: 14px;
    -webkit-transition: all 0.3 ease;
    transition: all 0.3 ease;
    cursor: pointer;
    }

    .form .message {
    margin: 15px 0 0;
    color: #b3b3b3;
    font-size: 12px;
    }
    .form .message a {
    color: #4CAF50;
    text-decoration: none;
    }
    .form .register-form {
    display: none;
    }
    .container {
    position: relative;
    z-index: 1;
    max-width: 300px;
    margin: 0 auto;
    }
    .container:before, .container:after {
    content: "";
    display: block;
    clear: both;
    }
    .container .info {
    margin: 50px auto;
    text-align: center;
    }
    .container .info h1 {
    margin: 0 0 15px;
    padding: 0;
    font-size: 36px;
    font-weight: 300;
    color: #1a1a1a;
    }
    .container .info span {
    color: #4d4d4d;
    font-size: 12px;
    }
    .container .info span a {
    color: #000000;
    text-decoration: none;
    }
    .container .info span .fa {
    color: #EF3B3A;
    }
    body {

    font-family: "Roboto", sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    }

    #body {
        background-position: center top;
        background-size: 100% auto;
    }

</style>
<link rel="icon"> 
<meta http-equiv="refresh" content="<?php echo $RefreshTime; ?>">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<script src="../jquery/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</head>
<body background="../bg/background.jpg" id="body" style="background-repeat: no-repeat;background-attachment: fixed;">
<div align='center'>
    <h1 class="display-4" style="margin-top:50px;"><b><font style='font-family:impact'><?php echo $Name; ?></font></b></h1>
</div>
<br>
<div style="width: 100%; display: table;">
    <div style="display: table-row">
        <div style="width: 30%; display: table-cell;">
        
            <div class="form" style='width:100%; overflow: hidden; border-radius: 21px;'>
                <br><h2>CÓDIGO SALA: <?php echo $Code; ?></h2><br><br>
                <?php if($AutoCall == 0) {?>
                    <a href="../actions/callBall.php?GameId=<?php echo $Id; ?>"><button class="btn btn-primary"><b>PRÓXIMA BOLA</b></button></a>
                    
                    <br><br>
                <?php } else {echo "<br><br><br>"; } ?>
                <?php if($AutoCall == 0) {?>
                    <a href="../actions/turnOnAutoCall.php?GameId=<?php echo $Id; ?>"><button class="btn btn-warning"><b>CANTAR AUTOMATICAMENTE: DESLIGADO</b></button></a>
                    <br><br>
                <?php } ?>
                <?php if($AutoCall == 1) {?>
                    <a href="../actions/turnOffAutoCall.php?GameId=<?php echo $Id; ?>"><button class="btn btn-success"><b>CANTAR AUTOMATICAMENTE: LIGADO</b></button></a>
                    <br><br>
                <?php } ?>
                <a href="game.php?Code=<?php echo $Code; ?>"><button class="btn btn-info"><b>RECARREGAR</b></button></a>
                <br><br>
                <a href="menu.php"><button class="btn btn-danger"><b>VOLTAR AO MENU</b></button></a>
                <br><br>
                <br><br>
                <br><br>
                <br><br>
                <br><br>
                <br><br>
                <?php if ($Active == 1 && $Owner_Id == $_SESSION['User_Id']) {?>
                    <a href="../actions/finishGame.php?GameId=<?php echo $Id; ?>"><button class="btn btn-danger"><b>PARAR PARTIDA</b></button></a>
                <?php } elseif ($Active == 0 && $Owner_Id == $_SESSION['User_Id']) {?>
                    <a href="../actions/unfinishGame.php?GameId=<?php echo $Id; ?>"><button class="btn btn-success"><b>VOLTAR PARTIDA</b></button></a>
                <?php } elseif ($Active == 1 && $Owner_Id != $_SESSION['User_Id']) {?>
                    <font color='green'><b>PARTIDA ATIVA</b></font>
                <?php } elseif ($Active == 0 && $Owner_Id != $_SESSION['User_Id']) {?>
                    <font color='red'><b>PARTIDA PARADA</b></font>
                <?php } ?>

            </div> 
        </div>
        <div style="display: table-cell;"> 
            
            <div class="form" style='width:100%; border-radius: 21px; max-width: 98%!important;'>
                <br>
                <div>
                    <div id='balls' style="text-align: left ;"> 
                    <?php 

                    if ($resultBalls->num_rows > 0) {
                        echo "<h1>";
                        while($row = $resultBalls->fetch_assoc()) {
                            if ($row['Called'] == 0){
                                echo "<font>".$row['Number']. '</font>  ';
                            } else {
                                echo "ㅤ<font color ='green'><b>".$row['Number']. '</b></font>ㅤ';
                            }
                        }
                    }
                    ?>
                    </div>
                    
                </div>
 
            </div> 
        </div>

        <div style="display: table-cell;"> 
            
            <div class="form" style='width:100%; border-radius: 21px; max-width: 100%!important;'>
                <br>
                <font style='font-size: 35px; font-family:impact'>Última bola cantada</font>
                <div>
                    <div id='balls' style="text-align: left ;"> 
                    <?php echo "<font style='font-size: 350px; font-family:impact
                    '>";
                    if ($LastBall < 10){ echo "0"; }
                    echo $LastBall. "</font>"; ?>
                    </div>
                    
                </div>
 
            </div> 
        </div>
    </div>
</div>

</body>
<?php 
    if ($Active == 1){
        $myAudioFile = "../sounds/numero$LastBall.ogg";
        echo '<audio autoplay="true" style="display:none;">
            <source src="'.$myAudioFile.'" type="audio/wav">
        </audio>';
    }

?>


</html>