<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>

    .login-page {
    width: 360px;
    padding: 8% 0 0;
    margin: auto;
    }
    .form {
    position: relative;
    z-index: 1;
    background: #FFFFFF;
    max-width: 360px;
    margin: 0 auto 100px;
    padding: 45px;
    text-align: center;
    box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
    }
    .form input {
    font-family: "Roboto", sans-serif;
    outline: 0;
    background: #f2f2f2;
    width: 100%;
    border: 0;
    margin: 0 0 15px;
    padding: 15px;
    box-sizing: border-box;
    font-size: 14px;
    }
    .form button {
    font-family: "Roboto", sans-serif;
    text-transform: uppercase;
    outline: 0;
    width: 100%;
    border: 0;
    padding: 15px;
    font-size: 14px;
    -webkit-transition: all 0.3 ease;
    transition: all 0.3 ease;
    cursor: pointer;
    }

    .form .message {
    margin: 15px 0 0;
    color: #b3b3b3;
    font-size: 12px;
    }
    .form .message a {
    color: #4CAF50;
    text-decoration: none;
    }
    .form .register-form {
    display: none;
    }
    .container {
    position: relative;
    z-index: 1;
    max-width: 300px;
    margin: 0 auto;
    }
    .container:before, .container:after {
    content: "";
    display: block;
    clear: both;
    }
    .container .info {
    margin: 50px auto;
    text-align: center;
    }
    .container .info h1 {
    margin: 0 0 15px;
    padding: 0;
    font-size: 36px;
    font-weight: 300;
    color: #1a1a1a;
    }
    .container .info span {
    color: #4d4d4d;
    font-size: 12px;
    }
    .container .info span a {
    color: #000000;
    text-decoration: none;
    }
    .container .info span .fa {
    color: #EF3B3A;
    }
    body {

    font-family: "Roboto", sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    }

    #body {
        background-position: center top;
        background-size: 100% auto;
    }

</style>
<link rel="icon"> 
<link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body background="../bg/background.jpg" id="body" style="background-repeat: no-repeat;background-attachment: fixed;">

<div class="login-page">

<div align='center'>
    <h1 class="display-4"><b>BRUBINGO</b></h1>
</div>
    <div class="form" border = 1 align= 'center' style='width:100%; overflow: hidden; border-radius: 21px;'>
        <form class="login-form" method="POST" action='../actions/createAccount.php'>

            <b>APELIDO</b><input type="text" name ='NickName' minlength="5" placeholder="APELIDO"/>
            <b>SENHA</b><input type="password" name='Password' minlength="3" placeholder="SENHA"/>
            <b>SENHA NOVAMENTE</b><input type="password" name='PasswordAgain' placeholder="SENHA NOVAMENTE"/>

            <button class="btn btn-success"><b>CRIAR CONTA</b></button>
        </form>
        <br>
        <button class="btn btn-danger" onclick="history.back()"><b>VOLTAR</b></button>
        <?php if (isset($_GET['erro']) && $_GET['erro'] == 1){ ?>
            <br><br>
            <font color="red"><b><h5>Apelido muito pequeno</h5></b></font>
        <?php } if (isset($_GET['erro']) && $_GET['erro'] == 2){ ?>
            <br><br>
            <font color="red"><b><h5>Senha muito pequena</h5></b></font>
        <?php } if (isset($_GET['erro']) && $_GET['erro'] == 3){ ?>
            <br><br>
            <font color="red"><b><h5>Senhas devem ser iguais</h5></b></font>
        <?php } ?>
    </div>

</div>


</body>

</html>
