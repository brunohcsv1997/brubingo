<?php 
    include_once "../configs/dbConfig.php";
    @session_start();

    if (!$_SESSION['User_Id']) {
        header('Location: pages/login.php');
    }

    $queryMyMatchs = "SELECT `Id`, 
        `Name`, 
        `Code`, 
        DATE_FORMAT(`CreatedAt`, '%d/%m/%Y %H:%i:%s') 'CreatedAt', 
        DATE_FORMAT(`FinishedAt`, '%d/%m/%Y %H:%i:%s') 'FinishedAt',
        CASE WHEN `FinishedAt` IS NULL THEN 1 ELSE 0 END 'Active'
        FROM games WHERE Owner_Id = $_SESSION[User_Id] ORDER BY 1 DESC LIMIT 10";

    $queryOtherMatchs = "SELECT `Id`, 
        `Name`, 
        `Code`, 
        DATE_FORMAT(`CreatedAt`, '%d/%m/%Y %H:%i:%s') 'CreatedAt', 
        DATE_FORMAT(`FinishedAt`, '%d/%m/%Y %H:%i:%s') 'FinishedAt',
        CASE WHEN `FinishedAt` IS NULL THEN 1 ELSE 0 END 'Active'
        FROM games WHERE Id IN (SELECT Game_Id FROM games_guests WHERE User_Id = $_SESSION[User_Id]) ORDER BY 1 DESC LIMIT 10";

    $resultMyMatchs = $conn->query($queryMyMatchs);
    $resultOtherMatchs = $conn->query($queryOtherMatchs);
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>

    .login-page {
    width: 20%;
    padding: 8% 0 0;
    margin: auto;
    }
    .form {
    position: relative;
    z-index: 1;
    background: #FFFFFF;
    max-width: 100%;
    margin: 0 auto 100px;
    padding: 45px;
    text-align: center;
    box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
    }
    .form input {
    font-family: "Roboto", sans-serif;
    outline: 0;
    background: #f2f2f2;
    width: 100%;
    border: 0;
    margin: 0 0 15px;
    padding: 15px;
    box-sizing: border-box;
    font-size: 14px;
    }
    .form button {
    font-family: "Roboto", sans-serif;
    text-transform: uppercase;
    outline: 0;
    width: 100%;
    border: 0;
    padding: 15px;
    font-size: 14px;
    -webkit-transition: all 0.3 ease;
    transition: all 0.3 ease;
    cursor: pointer;
    }

    .form .message {
    margin: 15px 0 0;
    color: #b3b3b3;
    font-size: 12px;
    }
    .form .message a {
    color: #4CAF50;
    text-decoration: none;
    }
    .form .register-form {
    display: none;
    }
    .container {
    position: relative;
    z-index: 1;
    max-width: 300px;
    margin: 0 auto;
    }
    .container:before, .container:after {
    content: "";
    display: block;
    clear: both;
    }
    .container .info {
    margin: 50px auto;
    text-align: center;
    }
    .container .info h1 {
    margin: 0 0 15px;
    padding: 0;
    font-size: 36px;
    font-weight: 300;
    color: #1a1a1a;
    }
    .container .info span {
    color: #4d4d4d;
    font-size: 12px;
    }
    .container .info span a {
    color: #000000;
    text-decoration: none;
    }
    .container .info span .fa {
    color: #EF3B3A;
    }
    body {

    font-family: "Roboto", sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    }

    #body {
        background-position: center top;
        background-size: 100% auto;
    }

</style>
<link rel="icon"> 
<link rel="stylesheet" href="../css/bootstrap.min.css">
<script src="../jquery/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</head>
<body background="../bg/background.jpg" id="body" style="background-repeat: no-repeat;background-attachment: fixed;">
<div align='center'>
    <h1 class="display-4" style="margin-top:50px;"><b><font style="font-family:impact">Olá, <?php echo $_SESSION['NickName']; ?></font></b></h1>
</div>
<br>
<div style="width: 100%; display: table;">
    <div style="display: table-row">
        <div style="width: 30%; display: table-cell;">
        
            <div class="form" style='width:100%; overflow: hidden; border-radius: 21px;'>
                
                <button class="btn btn-primary" data-toggle="modal" data-target="#createMatch"><b>CRIAR PARTIDA</b></button>
                <br><br><b>OU</b><br><br>
                <form method="POST" action="../actions/joinGame.php">
                    <input type='text' name='Code' placeholder="CÓDIGO DE PARTIDA">
                    <button class="btn btn-success"><b>ENTRAR</b></button>
                </form>
                <br><br>
                <?php 
                    if (isset($_GET['erro']) && $_GET['erro'] == 1){
                        echo '<font color="red"><b><h5>Não foi possível criar sala<br>Verifique os dados inseridos e tente novamente</h5></b></font>';
                    } else { echo "<br><br>"; }
                ?>
                <br><br>
                <a href="menu.php"><button class="btn btn-dark"><b>TROCAR SENHA</b></button></a>
                <br><br>
                <a href="menu.php"><button class="btn btn-info"><b>RECARREGAR</b></button></a>
                <br><br>
                <a href="../actions/logout.php"><button class="btn btn-danger"><b>SAIR</b></button></a>

            </div> 
        </div>
        <div style="display: table-cell;"> 
            
            <div class="form" style='width:100%; border-radius: 21px; max-width: 95%!important;'>
                
                <div style="display: table;">
                    <div style="display: table-row">
                        <div style="display: table-cell;">
                            <button type="button" class="btn btn-primary" id='btnMyMatchs' onclick="showMyMatchs()"><b>MINHAS PARTIDAS</b></button>
                        </div>
                        <div style="display: table-cell;">
                            <button type="button" class="btn btn-outline-success" id='btnOtherMatchs' onclick="showOtherMatchs()"><b>PARTIDAS QUE ENTREI</b></button>
                        </div>
                    </div>
                </div>
                <br>
                <div>
                    <div id='myMatchs'> 
                        <table class="table" style="width:100%">
                            <thead class="thead-dark">
                                <th>Nome</th>
                                <th>Código</th>
                                <th>Criação</th>
                                <th>Finalização</th>
                            </thead>
                            <tbody>
                                <?php if ($resultMyMatchs->num_rows > 0) {
                                        while($row = $resultMyMatchs->fetch_assoc()) {
                                            echo "<tr>
                                                <td style='vertical-align: middle;'><font style='font-family:impact'>$row[Name]</font></td>
                                                <td style='vertical-align: middle;'><a href='game.php?Code=$row[Code]'><button>$row[Code]</button></a></td>
                                                <td style='vertical-align: middle;'>$row[CreatedAt]</td>
                                                <td style='vertical-align: middle;'>$row[FinishedAt]</td>
                                            </tr>";
                                    }
                                } ?>
                            </tbody>

                        </table>
                        <button type="button" class="btn btn-warning"><b>VER TUDO</b></button>
                    </div>

                    <div id='otherMatchs' style="display: none"> 
                        <table class="table" style="width:100%">
                            <thead class="thead-dark">
                                <th>Nome</th>
                                <th>Código</th>
                                <th>Criação</th>
                                <th>Finalização</th>
                            </thead>
                            <tbody>
                                <?php if ($resultOtherMatchs->num_rows > 0) {
                                        while($row = $resultOtherMatchs->fetch_assoc()) {
                                            echo "<tr>
                                                <td style='vertical-align: middle;'><font style='font-family:impact'>$row[Name]</font></td>
                                                <td style='vertical-align: middle;'><a href='game.php?Code=$row[Code]'><button>$row[Code]</button></a></td>
                                                <td style='vertical-align: middle;'>$row[CreatedAt]</td>
                                                <td style='vertical-align: middle;'>$row[FinishedAt]</td>
                                            </tr>";
                                    }
                                } ?>
                            </tbody>

                        </table>
                        <button type="button" class="btn btn-warning"><b>VER TUDO</b></button>
                    </div>

                    
                </div>
 
            </div> 
        </div>
    </div>
</div>
<script>
    function showMyMatchs(){
        document.getElementById("btnMyMatchs").className = 'btn btn-primary';
        document.getElementById("btnOtherMatchs").className = 'btn btn-outline-success';
        var myMatchs = document.getElementById("myMatchs");
        var otherMatchs = document.getElementById("otherMatchs");
        myMatchs.style.display = "block";
        otherMatchs.style.display = "none";
    }

    function showOtherMatchs(){
        document.getElementById("btnMyMatchs").className = 'btn btn-outline-primary';
        document.getElementById("btnOtherMatchs").className = 'btn btn-success';
        var myMatchs = document.getElementById("myMatchs");
        var otherMatchs = document.getElementById("otherMatchs");
        myMatchs.style.display = "none";
        otherMatchs.style.display = "block";
    }

</script>

<!-- MODAL -->
<div class="modal fade" id="createMatch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">CRIAÇÃO DE PARTIDA</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form method="POST" action="../actions/createGame.php">
            <div class="modal-body">
                <b>NOME</b><input type="text" name="name" maxlength="40" minlength="3" style="width: 100%" placeholder="NOME DA PARTIDA">
                <br><br>
                <b>QUANTIDADE DE NÚMEROS</b><input type="number" name="qtyBalls" maxlength="40" min='10' max='100' value='90' style="width: 100%" placeholder="10 a 100">
                <br><br>
                <b>TEMPO PARA RECARREGAR</b><input type="number" name="refreshTime" maxlength="40" min='2' max='20' value='8' style="width: 100%" placeholder="2 a 20">
                <br><br>
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="autoCall" name="autoCall">
                    <label class="custom-control-label" for="autoCall">CANTAR BOLAS AUTOMATICAMENTE</label>
                </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Criar</button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>

</html>