<?php 

    @session_start();

    if (!$_SESSION['User_Id']) {
        header('Location: pages/login.php');
    } else {
        header('Location: pages/menu.php');
    }

?>

